﻿using UnityEngine;
using System.Collections;

public class LevelCreator : MonoBehaviour
{
    private GameObject _tilePos;
    private float _startUpPosY;
    private const float TILE_WIDTH = 0.15f;
    private int _heightLevel = 0;
    private GameObject _tempTile;

    private GameObject collectedTiles;
    private GameObject gameLayer;
    private GameObject bgLayer;
    

    void Start()
    {
        gameLayer = GameObject.Find("gameLayer");
        bgLayer = GameObject.Find("backgroundLayer");
        collectedTiles = GameObject.Find("tiles");

        //Create tiles in advanced.
        for (int i = 0; i < 21; i++)
        {
            GameObject tempg1 = Instantiate(Resources.Load("ground_left", typeof(GameObject))) as GameObject;
            tempg1.transform.parent = collectedTiles.transform.FindChild("gLeft").transform;

            GameObject tempg2 = Instantiate(Resources.Load("ground_middle", typeof(GameObject))) as GameObject;
            tempg2.transform.parent = collectedTiles.transform.FindChild("gMiddle").transform;

            GameObject tempg3 = Instantiate(Resources.Load("ground_right", typeof(GameObject))) as GameObject;
            tempg3.transform.parent = collectedTiles.transform.FindChild("gRight").transform;

            GameObject tempg4 = Instantiate(Resources.Load("blank", typeof(GameObject))) as GameObject;
            tempg4.transform.parent = collectedTiles.transform.FindChild("gBlank").transform;

        }

        collectedTiles.transform.position = new Vector2(-60f, -20f);

        _tilePos = GameObject.Find("startTilePosition");
        _startUpPosY = _tilePos.transform.position.y;

        FillScene();
    }

    private void FillScene()
    {
        for (int i = 0; i < 15; i++)
        {
            SetTile("middle");
        }

        SetTile("right");
    }

    public void SetTile(string type)
    {
        switch (type)
        {
            case "left":
                _tempTile = collectedTiles.transform.FindChild("gLeft").transform.GetChild(0).gameObject;
                break;

            case "right":
                _tempTile = collectedTiles.transform.FindChild("gRight").transform.GetChild(0).gameObject;
                break;

            case "middle":
                _tempTile = collectedTiles.transform.FindChild("gMiddle").transform.GetChild(0).gameObject;
                break;

            case "blank":
                _tempTile = collectedTiles.transform.FindChild("gBlank").transform.GetChild(0).gameObject;
                break;
        }

        _tempTile.transform.parent = gameLayer.transform;
        _tempTile.transform.position = new Vector2(_tilePos.transform.position.x + TILE_WIDTH,
            _startUpPosY + _heightLevel * TILE_WIDTH);

        _tilePos = _tempTile;

    }
}